import sys
import os
from myoreader.myoReader import MyoReader
from obspy import UTCDateTime, read, Trace, Stream
import numpy as np

version = "1.0.0"

####################################################################################
def exit_usage() : 
    print("Usage :\tpython " + os.path.basename(sys.argv[0]) + " srcPath.myo destPath.mseed")
    print("\tpython " + os.path.basename(sys.argv[0]) + " --version")
    sys.exit(0)

def exit_version() :
    print("myotomseed :" + str(version))
    print("myoreader :" + MyoReader.class_version)
    sys.exit(0)

def get_band(freq, total_time) :
    band = 'B'
    if total_time >= 10 :
        #Mesure longue (broad band)
        if  freq >= 1000 :
            band = 'F'
        elif freq >= 250 :
            band = 'C'
        elif freq >= 80 :
            band = 'H'
        elif freq >= 10 :
            band = 'B'
    else :
        #mesure courte (short period)
        if  freq >= 1000 :
            band = 'G'
        elif freq >= 250 :
            band = 'D'
        elif freq >= 80 :
            band = 'E'
        elif freq >= 10 :
            band = 'S'

    #No category for sampling < 10Hz
    if freq < 10 :
        if freq > 1 :
            band = 'M'
        elif freq > 0.1 :
            band = "L"
        elif freq > 0.01 :
            band = "V"
        elif freq > 0.001 :
            band = "U"
        elif freq > 0.00001 :
            band = "R"

    return(band)


####################################################################################


#Check argv
if len(sys.argv) == 1 :
    exit_usage()

if sys.argv[1] == "--version" and len(sys.argv) == 2 :
    exit_version()
elif len(sys.argv) != 3 :
    exit_usage()
    

if os.path.isfile(sys.argv[1]) == False :
    print("Bad source path")
    exit_usage()

parser = MyoReader()
parser.parse(sys.argv[1])

if os.path.isfile(sys.argv[2]) == True :
    os.remove(sys.argv[2])

#Create the mseed file
f = open(sys.argv[2], "x+")
f.close()

start_time = UTCDateTime(parser.date_time.strftime("%Y-%m-%dT%H:%M:%S.%f"))
print(start_time)

traces = []
for s in parser.sensors :
    freq = 1000000/parser.tick_time/s.saving_tick
    nbData = len(s.datas)
    total_time = parser.tick_time*nbData/1000000
    band = get_band(freq, total_time)
    data = np.array([float(x) for x in list(s.datas.values())], dtype=np.float32)
    header = {'network': '', 'station': parser.station_name[0:4] , 'location': '',
         'channel': band + 'H' + str((s.id+1)%4), 'npts': nbData, 'sampling_rate': freq,
         'mseed': {'dataquality': 'D'}, "starttime":start_time}
    traces.append(Trace(data=data,header=header))


sismic = Stream(traces)
sismic.write(sys.argv[2], format='MSEED', encoding=4, reclen=512)
