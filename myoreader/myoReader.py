from io import SEEK_SET
from myoreader.sensor import *
import os
import shutil
import sys
from datetime import datetime, tzinfo
import struct
import datetime


class MyoReader :
    #Constant
    class_version = "1.1.0"
    station_name_offset = 40
    sensor_header_size = 26


    def __init__(self, path = "") :
        #Working variables
        self.path = path
        self.sensors_header_offset = -1
        self.datas_offset = -1

        #File infos
        self.crc = -1
        self.version = -1
        self.date_time = -1
        self.uid = -1
        self.tick_start = -1
        self.tick_time = -1
        self.nb_sensors = -1
        self.station_name = ""
        self.sensors = []
        self.datas = {}

    def parse_crc(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        self.crc = MyoReader.get_uint32(f.read(4))
        f.close
        return(self.crc)

    def parse_version(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(4,SEEK_SET)
        self.version = MyoReader.get_uint16(f.read(2))
        f.close
        return(self.version)
    
    def parse_started_time(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(6,SEEK_SET)
        timestamp = MyoReader.get_uint64(f.read(8))
        subsec = MyoReader.get_uint32(f.read(4))
        self.date_time = datetime.datetime.fromtimestamp(timestamp)
        self.date_time = self.date_time.replace(microsecond=subsec)
        f.close
        return(self.date_time)
    
    def parse_uid(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(18,SEEK_SET)
        self.uid = MyoReader.get_uint32(f.read(4))
        f.close
        return(self.uid)
    
    def parse_first_tick(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(22,SEEK_SET)
        self.tick_start = MyoReader.get_uint64(f.read(8))
        f.close
        return(self.tick_start)
    
    def parse_tick_time(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(30,SEEK_SET)
        self.tick_time = MyoReader.get_uint64(f.read(8))
        f.close
        return(self.tick_time)
    
    def parse_nb_sensors(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(38,SEEK_SET)
        self.nb_sensors = MyoReader.get_uint16(f.read(2))
        f.close()
        return self.nb_sensors
    
    def parse_station_name(self):
        if(os.path.isfile(self.path) == False):
            return -1
        f = open(self.path, "rb")
        f.seek(self.station_name_offset,SEEK_SET)
        self.station_name  = ""
        while c := f.read(1):
            if c == b'\0' :
                break
            self.station_name += c.decode("ascii")
        self.station_name =  self.station_name
        f.close()
        return self.station_name

    def parse_sensors(self):
        if(os.path.isfile(self.path) == False):
            return -1
    
        if self.parse_nb_sensors() == -1 :
            return -1

        offset = self.parse_sensor_header_offset()

        f = open(self.path, "rb")

        f.seek(offset,SEEK_SET)
        self.sensors.clear()
        for i in range(self.nb_sensors) :
            s = Sensor()
            s.id = MyoReader.get_uint16(f.read(2))
            s.saving_tick = (MyoReader.get_uint64(f.read(8)))
            s.calibration[0] = MyoReader.get_float(f.read(4))
            s.calibration[1] = MyoReader.get_float(f.read(4))
            s.coefs[0] = MyoReader.get_float(f.read(4))
            s.coefs[1] = MyoReader.get_float(f.read(4))
            self.sensors.append(s)
        return(self.sensors)

    def parse_datas(self) : 
        if(os.path.isfile(self.path) == False):
            return -1

        #Parse needed info
        if self.datas_offset == -1 :
            self.parse_datas_offset()
        if self.tick_start == -1 :
            self.parse_first_tick()
        if self.nb_sensors == -1 or len(self.sensors) <= 0 :
            self.parse_sensors()
        if self.date_time == -1 :   
            self.parse_started_time()
        if self.tick_time == -1 :
            self.parse_tick_time()

        f = open(self.path, "rb")
        #Move until the first metrix
        f.seek(self.datas_offset, SEEK_SET)
        dateTime = self.date_time + datetime.timedelta(microseconds=(self.tick_start*self.tick_time))
        firstSensor = MyoReader.get_uint16(f.read(2))
        data = MyoReader.get_int32(f.read(4))*self.sensors[firstSensor].coefs[1] + self.sensors[firstSensor].coefs[0]
        self.datas[dateTime] = [(data)]
         #Ajout de la donnée triée par sensor
        self.sensors[firstSensor].datas[dateTime] = data

        while True :
            b = f.read(2)
            #EOF
            if not b :
                break
            id   = MyoReader.get_uint16(b)
            data = MyoReader.get_int32(f.read(4))*self.sensors[id].coefs[1] + self.sensors[id].coefs[0]
            #Ajout de la donnée triée par sensor
            self.sensors[id].datas[dateTime] = data

            if id == firstSensor :
                dateTime += datetime.timedelta(microseconds=(self.tick_time))
                self.datas[dateTime] = [(data)]
            else :
                self.datas[dateTime].append((data))
        return(self.datas)
            
  

    def parse_datas_offset(self) : 
        if(os.path.isfile(self.path) == False):
            return -1
        if self.nb_sensors == -1 :
            self.parse_nb_sensors()
        if self.sensors_header_offset == -1 :
            self.parse_sensor_header_offset()
        self.datas_offset = self.sensors_header_offset + self.nb_sensors*self.sensor_header_size
        return(self.datas_offset)

    def parse_sensor_header_offset(self) : 
        if(os.path.isfile(self.path) == False):
            return -1

        f = open(self.path, "rb")
        #Décalage jusqu'au nombre de voie
        f.seek(self.station_name_offset, SEEK_SET)
        i = 0
        while c := f.read(1):
            if c == b'\0':
                break
            i += 1
        f.close()
        self.sensors_header_offset = self.station_name_offset+i+1
        return(self.sensors_header_offset)

    def parse(self, path = "") :
        #Check if one of path is correct
        if path == "" and os.path.isfile(self.path) == False :
            return -1

        #Assign the new path and check if it's correct
        self.path = path
        if os.path.isfile(self.path) == False :
            return -1

        self.parse_crc()
        self.parse_version()
        self.parse_datas()
        self.parse_uid()
        self.parse_station_name()

    @staticmethod
    def get_uint8(buff):
        return(struct.unpack('<B', buff)[0])
    
    @staticmethod
    def get_int8(buff):
        return(struct.unpack('<b', buff)[0])
    
    @staticmethod
    def get_uint16(buff):
         return(struct.unpack('<H', buff)[0])
    
    @staticmethod
    def get_int16(buff):
          return(struct.unpack('<h', buff)[0])
    
    @staticmethod
    def get_uint32(buff):
         return(struct.unpack('<L', buff)[0])
    
    @staticmethod
    def get_int32(buff):
          return(struct.unpack('<l', buff)[0])
    
    @staticmethod
    def get_uint64(buff):
        return(struct.unpack('<Q', buff)[0])

    @staticmethod
    def get_int64(buff):
          return(struct.unpack('<q', buff)[0])

    @staticmethod
    def get_float(buff):
        return(struct.unpack('<f', buff)[0])
    
    @staticmethod
    def get_double(buff):
        return(struct.unpack('<d', buff)[0])