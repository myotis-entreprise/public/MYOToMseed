# Myotomseed

Convert a .myo file to the mini seed file format


# Getting started

To use this class, you need obspy. Install this package with pip:
````
pip install obspy
````

Clone the project with git or download it :
````
git clone https://gitlab.com/myotis-entreprise/public/myotomseed.git
````

Call the script with the .myo file path as the first argument and the destination mseed path as the second 
argument:
````
python myotomseed.py my_binary_file.myo my_mseed_file.mseed
````


# Channel naming
As channel naming and station naming are strictely limited in the mseed standard, the script will take only the first 5 characters of the 
station name of the .myo file.

For orientation, channel are packed of 3 and the convention is the "Orthogonal components but non traditional orientations" ([ref](https://ds.iris.edu/ds/nodes/dmc/data/formats/seed-channel-naming/))

For example, channel with id 0 to 2 become xx1, xx2, xx3        
            channel with id 3 to 5 become xx1, xx2, xx3


# Location
As the location in not included in the .myo file, no location information is 
added to the mseed file. You can manualy add it with the log file of 
the myotis station and datalogger





